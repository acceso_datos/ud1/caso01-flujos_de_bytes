package flujobytes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class App {

	public static void main(String[] args) {

		File f = new File("datos.dat");

//		ejemploEscritura(f);

//		ejemploLectura(f);

		// Actividades previas
		
		// Si tenemos un archivo grande, necesitaremos leerlo en bloques de x bytes cada
		// vez. ¿Qué tamaño de bloque sería mejor?
		// Mide el tiempo que tarda en leerse el fichero para diferentes tamaños de
		// bloque y compruébalo.
//		actividadPreviaMideTiempo();   // Podrías ahora hacer pruebas usando BufferedInputStream		

		// Crea un método que permita copiar un fichero en otro. El método tendrá 2
		// parámetros: el fichero origen y el destino.
		actividadPreviaCopiaFichero(f, new File("datos.dat.bak"));

	}

	private static void ejemploEscritura(File f) {
		// Ejemplo: Escritura del abecedario en un fichero binario
		try (FileOutputStream fos = new FileOutputStream(f, false)) {
			for (int i = 65; i < 91; i++) {
				fos.write(i);
			}
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}

		// -------------------------------------------------------------------------------------

		// Ejemplo: Escritura de una cadena en archivo binario
		String cadena = "\nACCESO A DATOS - DAM - BATOI";
		try (FileOutputStream fos = new FileOutputStream(f, true)) {
			fos.write(cadena.getBytes());
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}
		System.out.println("Fin escritura fichero.");
	}

	private static void ejemploLectura(File f) {
		// Lectura byte a byte
		System.out.println("* Lectura byte a byte");
		try (FileInputStream fis = new FileInputStream(f)) {
			while (fis.available() > 0) {
				int r = fis.read();
				System.out.print((char) r);
			}
			System.out.println();
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}

		// Lectura de golpe
		System.out.println("* Lectura de golpe");
		try (FileInputStream fis = new FileInputStream(f)) {
			byte[] lectura = fis.readAllBytes();
//			byte[] lectura = new byte[(int) f.length()];
			fis.read(lectura);
			System.out.println(new String(lectura));
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}

		// Lectura en bloque
		// Prueba diferentes tamaño de bloque. ¿Observas algún problema en este código?
		System.out.println("* Lectura en bloque");
		int tamBloque = 10;
		byte[] lecturaBloque = new byte[tamBloque];
		try (FileInputStream fis = new FileInputStream(f)) {
			while (fis.available() > 0) {
				lecturaBloque = new byte[tamBloque];
				int bytesLeidos = fis.read(lecturaBloque);
				// System.out.println(bytesLeidos);
				System.out.print(new String(lecturaBloque));
			}
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}

	}

	private static void actividadPreviaMideTiempo() {

		long tiempoInicio = System.currentTimeMillis();
		
		File fg = new File("ficherogrande.txt");
		int tamBloque = 1000;
		System.out.println("tambloque: " + tamBloque);
		byte[] lecturaBloque = new byte[tamBloque];
		try (FileInputStream fis = new FileInputStream(fg)) {
			while (fis.available() > 0) {
				lecturaBloque = new byte[tamBloque];
				int bytesLeidos = fis.read(lecturaBloque);
				// System.out.println(new String(lecturaBloque));
			}
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}
		
		long tiempoFin = System.currentTimeMillis();
		System.out.println("Tarda " + (tiempoFin - tiempoInicio) + " ms");

	}

	private static void actividadPreviaCopiaFichero(File fOrigen, File fDestino) {

		// Todo de golpe
		try (FileInputStream fis = new FileInputStream(fOrigen);
				FileOutputStream fos = new FileOutputStream(fDestino);) {
			// byte[] contenido = new byte[(int) fOrigen.length()];
			byte[] contenido = fis.readAllBytes();
			fis.read(contenido);
			fos.write(contenido);
			System.out.println("Fichero copia exitosamente...");
		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}

		// En bloque
		final int tamBloque = 10;
		try (FileInputStream fis = new FileInputStream(fOrigen);
				FileOutputStream fos = new FileOutputStream(fDestino);) {
			while (fis.available() > 0) {
				byte[] contenido = new byte[tamBloque];
				int bytesLeidos = fis.read(contenido);
//				if (bytesLeidos < tamBloque) {
//					contenido = Arrays.copyOf(contenido, bytesLeidos);
//				}
//				contenido = (bytesLeidos < tamBloque)? Arrays.copyOf(contenido, bytesLeidos): contenido;
//				fos.write(contenido);
				fos.write(contenido, 0, bytesLeidos);
			}
			System.out.println("Fichero copia exitosamente...");

		} catch (FileNotFoundException ex) {
			System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error durante el tratamiento del fichero: " + ex.getMessage());
		}

	}

}
