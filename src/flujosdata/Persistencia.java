package flujosdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import modelo.Partida;

import java.io.DataInputStream;
import java.io.DataOutputStream;

public class Persistencia {
	private File archivo;

	public Persistencia() {
		archivo = new File("partidas.dat");
	}
	
	private Partida leerRegistro(DataInputStream dis) throws IOException {
		Partida partida = new Partida();
		partida.setIdJudador(dis.readInt());
		partida.setNomJugador(dis.readUTF());
		partida.setPuntos(dis.readLong());
		partida.setTiempo(dis.readDouble());
		return partida;
	}

	public void guardar(Partida partida) throws IOException {
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(archivo, true));
		dos.writeInt(partida.getIdJudador());
		dos.writeUTF(partida.getNomJugador());
		dos.writeLong(partida.getPuntos());
		dos.writeDouble(partida.getTiempo());
		dos.close();
	}

	public Partida leer(int idJugador) throws IOException {
		FileInputStream fis = new FileInputStream(archivo);
		DataInputStream dis = new DataInputStream(fis);
		Partida partida = null;
		boolean encontrado = false;
		while (fis.available() > 0 && !encontrado) {
			partida = leerRegistro(dis);
			if (partida.getIdJudador() == idJugador)
				encontrado = true;
		}
		fis.close();
		dis.close();
		return partida;
	}

	// Ejercicio1: debe devolver todos los registros de partidas del 
	// jugador que se pasa como parámetro
	public List<Partida> leerTodos(int idJugador) throws IOException {
		List<Partida> partidas = new ArrayList<>();
		FileInputStream fis = new FileInputStream(archivo);
		DataInputStream dis = new DataInputStream(fis);
		while (fis.available() > 0) {
			Partida p = leerRegistro(dis);
			if (p.getIdJudador() == idJugador) {
				partidas.add(p);
			}
		}
		fis.close();
		dis.close();
		return partidas;
	}

	// Ejercicio2: debe devolver el registro cuya puntuación es mayor.
	// En caso de igualdad se debe mirar el menor tiempo.
	// En caso de igualdad de tiempo, devolverá el primero encontrado.
	public Partida leerMejorPuntuacion() throws IOException {
		return null;
	}

	// Ejercicio3: debe devolver el registro con mayor puntuación del 
	// jugador que se pasa como parámetro
	// En caso de igualdad se debe mirar el menor tiempo.
	// En caso de igualdad de tiempo, devolverá el primero encontrado.
	public Partida leerMejorPuntuacion(int idJugador) throws IOException {
		return null;
	}


}